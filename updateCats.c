///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"

extern struct cat catdata[MAX_CAT];
extern int currentcatnum;

void updateCatName(int index, char newName []){
	int duplicate = 0;
        for (int i = 0; i < MAX_CAT; i++){
            if ( strcmp(catdata[i].name, newName) == 0 ){
            printf("Already a name \n");
            duplicate = 1;
            }
        }
        if ( duplicate < 1 ){
            strcpy(catdata[index].name, newName);
        }
}

void fixCat(int index){
    catdata[index].isfixed = true;
}

void updateCatWeight(int index, float newWeight){
        if (newWeight > 0 ){
                catdata[index].weight = newWeight;
        }
        else{
        printf("Cat needs to be heavier");
        }
}
