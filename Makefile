### University of Hawaii, College of Engineering
### @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Patrick Manuel <pamanuel@hawaii.edu>
### @date 5_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = main
all: $(TARGET)

addCats.o: addCats.c 
	$(CC) $(CFLAGS) -c addCats.c
updateCats.o: updateCats.c 
	$(CC) $(CFLAGS) -c updateCats.c
reportCats.o: reportCats.c
	$(CC) $(CFLAGS) -c reportCats.c
deleteCats.o: deleteCats.c 
	$(CC) $(CFLAGS) -c deleteCats.c
catDatabase.o: catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.h

main.o: main.c addCats.c updateCats.c reportCats.c deleteCats.c catDatabase.h
	$(CC) $(CFLAGS) -c main.c

##main: main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o
##	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o updateCats.o reportCats.o deleteCats.o catDatabase.o

clean:
	rm -f $(TARGET) *.o

