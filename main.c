///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "addCats.c"
#include "updateCats.c"
#include "reportCats.c"
#include "deleteCats.c"
#include "catDatabase.h"

struct cat catdata[MAX_CAT];
int currentcatnum = 0;

int main(){
	
	addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
	addCat( "Milo", MALE, MANX, true, 7.0 ) ;	
	addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
	addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
	addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
	addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
	
	printAllCats();
        printf("\n");
        
	int kali = findCat( "Kali" ) ;
	updateCatName( kali, "Chili" ) ; // this should fail
	printCat( kali );
	printf("\n");
	
	updateCatName( kali, "Capulet" ) ;
	updateCatWeight( kali, 9.9 ) ;
	fixCat( kali ) ;

	printCat( kali );
	printf("\n");
	printAllCats();
	printf("\n");

	deleteCats();
	printAllCats();
	printf("\n");
	
	deleteCat( 2 );
	printCat( 2 );
}
