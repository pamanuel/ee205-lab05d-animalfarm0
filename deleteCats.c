///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"

extern struct cat catdata[MAX_CAT];
extern int currentcatnum;

void deleteCats() {
    
    for (int i = 0; i < MAX_CAT; i++){
        strcpy( catdata[i].name, " " );
        catdata[i].gender  = 0        ;
        catdata[i].breed   = 0        ;
        catdata[i].isfixed = false    ;
        catdata[i].weight  = 0.0      ;
        }
}

void deleteCat(int index){
    for (int i = 0; i < MAX_CAT; i++){
    	if (i == index){
        strcpy( catdata[index].name, " " );
        catdata[index].gender  = 0        ;
        catdata[index].breed   = 0        ;
        catdata[index].isfixed = false    ;
        catdata[index].weight  = 0.0      ;
        }
     }
}

