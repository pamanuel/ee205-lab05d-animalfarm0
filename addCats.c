///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"

extern struct cat catdata[MAX_CAT];
extern int currentcatnum;
int addCat(  char name[],  enum Gender gender, enum Breed breed, bool isfixed, float weight){

do{
    if (strlen(name) > MAX_CAT_NAMES){
        printf("Name is too long bud");
        break;
    }
    if (weight <= 0){
        printf("Nah thats too light");
        break;
    }
    if (currentcatnum > MAX_CAT){
        printf("We have too many cats");
        break;
    }
    for (int i = 0; i < MAX_CAT; i++){
    	if ( strcmp(catdata[i].name, name) == 0 ) {
    	printf("Name already taken");
    	break;
    	}
    break;
    } 
    strcpy( catdata[currentcatnum].name, name );
    catdata[currentcatnum].gender  = gender    ;
    catdata[currentcatnum].breed   = breed     ;
    catdata[currentcatnum].isfixed = isfixed   ;
    catdata[currentcatnum].weight  = weight    ;
    currentcatnum +=1;
    break;
    } while(true);
    return 0;
}


  
    
    

