///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#define MAX_CAT       (10)
#define MAX_CAT_NAMES (32)
#include <stdbool.h>

#ifndef CATDATABASE_H
#define CATDATABASE_H
enum Gender {UNKNOWN_GENDER, MALE, FEMALE};
enum Breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


struct cat {

    char         name[MAX_CAT_NAMES];
    enum Gender  gender             ;
    enum Breed   breed              ;
    bool         isfixed            ;
    float        weight             ;
};
#endif
