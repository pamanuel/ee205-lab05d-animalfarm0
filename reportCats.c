///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "catDatabase.h"

extern struct cat catdata[MAX_CAT];
extern int currentcatnum;

void printCat( int index ){
    if ( index == 0){
        printf("AnimalFarm0: Bad Cat");
        }
    else if ( index > MAX_CAT){
        printf("AnimalFarm0: Bad Cat");
        }
    else { 
    printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]", 
        index,
        catdata[index].name,
        catdata[index].gender,
        catdata[index].breed,
        catdata[index].isfixed,
        catdata[index].weight);
    }
}
void printAllCats( ){
    for ( int i = 0 ; i  < currentcatnum ; i++){
        printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f] \n",
        i, 
        catdata[i].name,
        catdata[i].gender,
        catdata[i].breed,
        catdata[i].isfixed ,
        catdata[i].weight);
    }   
}

int findCat(char name[MAX_CAT_NAMES]){
    for (int i = 0; i < MAX_CAT; i++){
    	if ( strcmp(catdata[i].name, name) == 0 ) {
 	return i;
 	break;
 	}
    }
    return 0;
}
